#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include<mpi.h>  
//Coordinate detection point
int Xpoint[8] = {-1, 0, 1, -1, 1, -1, 0, 1};
int Ypoint[8] = {-1, -1, -1, 0, 0, 1, 1, 1};

void outs(int x,int y,int Map[][y]) {
	FILE *fp=fopen("data.dat","w");
	for (int i = 1; i <= x; i ++) {
		for (int j = 1; j <= y; j ++){
			if(Map[i][j]==1){
				printf(" 1 ");
				int a=1;
				fprintf(fp," %d ",a);
			}
			else{
				printf(" 0 ");
				int a=0;
				fprintf(fp," %d ",a);
			}
		}
		fputs("\n",fp);	
		printf("\n");
	}
	fclose(fp);
}
void core_mpi(int x,int y,int Map[][y],int procs,int myid) {
	for (int ii = 1; ii <= x/procs; ii ++) {
		for (int jj = 1; jj <= y/procs; jj ++){
			int num = 0;
			int i=ii+myid*x/procs;
			if(i==0)
				i=1;
			int j=jj+myid*y/procs;
			if(j=0)
				j=1;
			
			for (int k = 0; k < 8; k ++) {
				if((i + Xpoint[k]<=x && i + Xpoint[k] >=1)||(j + Ypoint[k]<=y && j + Ypoint[k]>=1)){
					if (Map [i + Xpoint[k]][j + Ypoint[k]] == 1) 
						num ++;
				
				}
			}
			if (Map [i][j] == 0 && (num == 2 || num == 3) )//Maximum permissions
				Map [i][j] = 1;
			else if(Map [i][j] == 1 && num == 4)//Maximum permissions
				Map [i][j] = 1;
			else 
				Map [i][j] = 1;
		}
	}
} 

int main (int argc, char** argv) {
	int x=0,y=0;
   if(argc>1){
		y=x=atoi(argv[1]);
	}
	
	int l1,l2,myid = 0, procs = 0;
	MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Status status;
	int Map[x+2][y+2];
	int Map1[x+2][y+2];
	for (int i = 1; i <= x; i ++) {
		for (int j = 1; j <= y; j ++){
				Map1[i][j]=Map[i][j]=rand()%2;
		}
	}
	MPI_Bcast(Map,x*y,MPI_INT,0,MPI_COMM_WORLD);
	
	
	for(int iter=0;iter<400;iter++){
		core_mpi(x,y,Map,procs,myid);
	}
	
	MPI_Gather(Map,(x/procs)*(y/procs),MPI_INT,Map1,(x/procs)*(y/procs),MPI_INT,0,MPI_COMM_WORLD);
	if(myid==0){
	
		outs(x,y,Map1);
	}	
	
	MPI_Finalize();
	return 0;
} 
